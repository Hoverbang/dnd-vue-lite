import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        charList: []
    },
    getters: {
        getChars: state => {
            return state.charList;
        },

        getChar: (state) => (id) => {
            return state.charList.find(charList => charList.id === id)
        }
    },
    mutations: {
        changeChars: (state, payload) => {
            state.charList = payload
        },
    }
    ,
    actions: {
        setChars: async (context, payload) => {
            let {data} = await axios.get('/json/characters.json');
            context.commit('changeChars', data)
        }
    }
})
